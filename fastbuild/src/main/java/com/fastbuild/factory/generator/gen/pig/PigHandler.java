package com.fastbuild.factory.generator.gen.pig;

import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.IGenHandler;

public class PigHandler implements IGenHandler {

    private AppConfig app;

    private AbstractFormat[] genList;

    public PigHandler(AppConfig app) {
        this.app = app;
        this.genList = new AbstractFormat[] {
                new PigServerFormat(this.app),
                new PigWebFormat(this.app),
        };
    }

    @Override
    public void gen() throws Exception {
        for (AbstractFormat gen : genList) {
            gen.gen();
        }
    }

}
