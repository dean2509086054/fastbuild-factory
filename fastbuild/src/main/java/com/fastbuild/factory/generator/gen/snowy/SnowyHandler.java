package com.fastbuild.factory.generator.gen.snowy;

import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.IGenHandler;

public class SnowyHandler implements IGenHandler {

    private AppConfig app;

    private AbstractFormat[] genList;

    public SnowyHandler(AppConfig app) {
        this.app = app;
        this.genList = new AbstractFormat[] {
                new SnowyServerFormat(this.app),
                new SnowyWebFormat(this.app),
        };
    }

    @Override
    public void gen() throws Exception {
        for (AbstractFormat gen : genList) {
            gen.gen();
        }
    }

}
