package com.fastbuild.factory.generator.gen.snowy;

import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.common.FileHelper;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.text.CaseUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Snowy单体服务端代码格式化
 *
 * @author fastbuild@163.com
 */
public class SnowyServerFormat extends AbstractFormat {

    private final String GEN_ID = "snowy#server";

    public SnowyServerFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.SNOWY.equals(app.getAppId());
    }

    @Override
    protected void dependency() {}

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = this.getSrcPath();
        File srcFile = new File(srcPath);
        File destRoot = new File(project.getServerRootPath());

        List<String> exclude = this.getExcludeFile();
        Map<String, String> replaceDirMap = this.getReplaceDirMap();
        Map<String, String> replaceFileMap = this.getReplaceFileMap();

        FileHelper.copyDirectory(srcFile, destRoot, new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !exclude.contains(file.getName());
            }
        }, replaceDirMap, replaceFileMap);

        this.fileContentFormat(destRoot);
    }

    /**
     * 格式化文件内容
     *
     * @param destRoot
     * @throws IOException
     */
    private void fileContentFormat (File destRoot) throws IOException {
        final String classNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), true, new char[] { '-', '_' });
        final String varNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), false, new char[] { '-', '_' });

        FileFormatter javaFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("java"));
        javaFormatter.replaceAll("vip.xiaonuo", project.getPackagePrefix());
        javaFormatter.replaceAll("Snowy", classNamePrefix);
        javaFormatter.replaceAll("snowy", varNamePrefix);
        javaFormatter.format();

        FileFormatter pomFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("xml"));
        pomFormatter.replaceAll("vip.xiaonuo", project.getPackagePrefix());
        pomFormatter.replaceAll("snowy", varNamePrefix);
        pomFormatter.format();

        FileFormatter ymlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("yml"));
        ymlFormatter.replaceAll("Snowy", project.getProjectName());
        ymlFormatter.replaceAll("snowy", varNamePrefix);
        ymlFormatter.format();

        FileFormatter vmFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("vm"));
        vmFormatter.replaceAll("vip.xiaonuo", project.getPackagePrefix());
        vmFormatter.replaceAll("snowy", varNamePrefix);
        vmFormatter.format();

        FileFormatter sqlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("sql"));
        sqlFormatter.replaceAll("snowy", project.getProjectName());
        sqlFormatter.format();

        FileFormatter factoriesFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("factories"));
        factoriesFormatter.replaceAll("vip.xiaonuo", project.getPackagePrefix());
        factoriesFormatter.format();
    }

    private List<String> getExcludeFile () {
        List<String> exclude = new ArrayList<>();
        exclude.add(".git");
        exclude.add(".github");
        exclude.add(".idea");
        exclude.add("_web");
        exclude.add("snowy-admin-web");
        return exclude;
    }

    private Map<String, String> getReplaceDirMap () {
        Map<String, String> replaceDirMap = new LinkedHashMap<>();
        replaceDirMap.put("snowy-", project.getProjectName() + "-");
        replaceDirMap.put("xiaonuo-", project.getProjectName() + "-");
        replaceDirMap.put("src/main/java/vip/xiaonuo", "src/main/java/" + project.getPackagePrefix().replaceAll("\\.", "/"));
        return replaceDirMap;
    }

    private Map<String, String> getReplaceFileMap () {
        final String classNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), true, new char[] { '-', '_' });
        Map<String, String> replaceFileMap = new LinkedHashMap<>();
        replaceFileMap.put("Snowy", classNamePrefix);
        return replaceFileMap;
    }

    private String getSrcPath () {
        if (FactoryConst.server.CLOUD.equals(this.project.getServerMode())) {
            return properties.getFactorySnowyCloudPath();
        } else {
            return properties.getFactorySnowyVuePath();
        }
    }

}
