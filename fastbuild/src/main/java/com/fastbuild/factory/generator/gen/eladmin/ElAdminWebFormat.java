package com.fastbuild.factory.generator.gen.eladmin;

import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;

public class ElAdminWebFormat extends AbstractFormat {

    private final String GEN_ID = "eladmin#web";

    public ElAdminWebFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.ELADMIN.equals(app.getAppId());
    }

    @Override
    protected void dependency() throws Exception {
    }

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = properties.getFactoryElAdminWebPath();
        File srcFile = new File(srcPath);

        String destPath = project.getWorkPath() + File.separator + project.getUiName();
        File destRoot = new File(destPath);

        FileUtils.copyDirectory(srcFile, destRoot, FileFilterUtils.trueFileFilter());
    }
}
