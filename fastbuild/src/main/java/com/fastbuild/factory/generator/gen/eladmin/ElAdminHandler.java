package com.fastbuild.factory.generator.gen.eladmin;

import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.IGenHandler;

public class ElAdminHandler implements IGenHandler {

    private AppConfig app;

    private AbstractFormat[] genList;

    public ElAdminHandler(AppConfig app) {
        this.app = app;
        this.genList = new AbstractFormat[] {
                new ElAdminServerFormat(this.app),
                new ElAdminWebFormat(this.app),
        };
    }

    @Override
    public void gen() throws Exception {
        for (AbstractFormat gen : genList) {
            gen.gen();
        }
    }

}
