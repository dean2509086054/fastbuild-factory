
const appList = [{
  appId: 'ruoyi',
  appType: 'basic-platform',
  title: '若依管理系统',
  link: 'http://doc.ruoyi.vip/',
  desc: 'RuoYi 是一个 Java EE 企业级快速开发平台，基于经典技术组合（Spring Boot、Spring Security、MyBatis、Jwt、Vue），内置模块如：部门管理、角色用户、菜单及按钮授权、数据权限、系统参数、日志管理、代码生成等。在线定时任务配置；支持集群，支持多数据源，支持分布式事务。。',
  preview: {
    enable: true,
    web: 'http://vue.ruoyi.vip',
    h5: '/static/preview/ruoyi-mobile-qr.png'
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' }
  ]
}, {
  appId: 'snowy',
  appType: 'basic-platform',
  title: 'Snowy权限管理系统',
  link: 'https://gitee.com/xiaonuobase/snowy',
  desc: 'Snowy是一款基于国产密码算法后台权限管理系统，其中采用了SM2、SM3、SM4及签名验签，软件层面完全符合等保测评要求，让更多的人认识密码，使用密码。技术框架与密码结合，让前后分离“密”不可分。',
  preview: {
    enable: true,
    web: 'https://snowy.xiaonuo.vip',
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
    { type: 'serverMode',  value: ['single', 'cloud'], msg: '服务端请选择单应用！'},
    { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
    { type: 'webUI',  value: ['antd'], msg: '请选择Ant Design前端组件！'},
  ]
}, {
  appId: 'ruoyiplus',
  appType: 'basic-platform',
  title: 'RuoYi-Plus管理系统',
  link: 'https://gitee.com/JavaLionLi/RuoYi-Vue-Plus',
  desc: '后台管理系统 重写RuoYi-Vue所有功能 集成 Sa-Token+Mybatis-Plus+Jackson+Xxl-Job+knife4j+Hutool+OSS 定期同步',
  preview: {
    enable: true,
    web: 'http://ryplus.srcw.top',
    h5: '/static/preview/ruoyi-mobile-qr.png'
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
    { type: 'serverMode',  value: ['single'], msg: '服务端请选择单应用！'},
    { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
    { type: 'webUI',  value: ['element'], msg: '请选择Element UI前端组件！'},
  ]
}, {
  appId: 'pig',
  appType: 'basic-platform',
  title: 'PIG权限管理系统',
  link: 'https://gitee.com/log4j/pig',
  desc: '基于 Spring Cloud 2021 、Spring Boot 2.7、 OAuth2 的 RBAC 权限管理系统，数据驱动视图的理念封装 element-ui，即使没有 vue 的使用经验也能快速上手。',
  preview: {
    enable: true,
    web: 'http://pigx.pig4cloud.com',
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
    { type: 'serverMode',  value: ['cloud'], msg: '服务端请选择微服务！'},
    { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
    { type: 'webUI',  value: ['element'], msg: '请选择Element UI前端组件！'},
  ]
}, {
  appId: 'eladmin',
  appType: 'basic-platform',
  title: 'EL-ADMIN 后台管理系统',
  link: 'https://el-admin.vip/',
  desc: '一个基于 Spring Boot 2.1.0 、 Spring Boot Jpa、 JWT、Spring Security、Redis、Vue的前后端分离的后台管理系统。',
  preview: {
    enable: true,
    web: 'https://el-admin.vip/demo',
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
    { type: 'serverMode',  value: ['single'], msg: '服务端请选择单应用！'},
    { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
    { type: 'webUI',  value: ['element'], msg: '请选择Element UI前端组件！'},
  ]
}, {
  appId: 'yudao',
  appType: 'basic-platform',
  title: '芋道管理系统',
  link: 'https://www.iocoder.cn/',
  desc: '芋道，以开发者为中心，打造中国第一流的快速开发平台，全部开源，个人与企业可 100% 免费使用。多种内置功能：系统功能、工作流程、支付系统、商城系统、基础设施等。',
  preview: {
    enable: false,
    web: 'http://dashboard.yudao.iocoder.cn',
  },
  rules: [
    { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
    { type: 'serverMode',  value: ['single'], msg: '服务端请选择单应用！'},
    { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
    { type: 'webUI',  value: ['element'], msg: '请选择Element UI前端组件！'},
  ]
// }, {
//   appId: 'jeecg',
//   appType: 'basic-platform',
//   title: 'JEECG BOOT 低代码开发平台',
//   link: 'http://www.jeecg.com/',
//   desc: 'JeecgBoot 是一款基于代码生成器的低代码平台！前后端分离架构 SpringBoot2.x，SpringCloud，Ant Design&Vue，Mybatis-plus，Shiro，JWT，支持微服务。强大的代码生成器让前后端代码一键生成，实现低代码开发! JeecgBoot 引领新的低代码开发模式(OnlineCoding-> 代码生成器-> 手工MERGE)， 帮助解决Java项目70%的重复工作，让开发更多关注业务。既能快速提高效率，节省研发成本，同时又不失灵活性！',
//   rules: [
//     { type: 'unique', value: 'basic-platform', msg: '基础平台只可选择一个，请先将应用列表中的其他基础平台移除！' },
//     { type: 'webFramework',  value: ['vue2'], msg: '请选择Vue2前端框架！'},
//     { type: 'webUI',  value: ['element'], msg: '请选择Element UI前端组件！'},
//   ]
// }, {
//   appId: 'spring-blade',
//   appType: 'basic-platform',
//   title: 'BladeX企业级开发平台',
//   link: 'https://gitee.com/smallc/SpringBlade/',
//   desc: 'SpringBlade 是一个由商业级项目升级优化而来的微服务架构，采用Spring Boot 2.6 、Spring Cloud 2021 等核心技术构建，完全遵循阿里巴巴编码规范。提供基于React和Vue的两个前端框架用于快速搭建企业级的SaaS多租户微服务平台。',
//   rules: [
//     { type: 'inUse', inUse: 0, msg: '功能开发中，敬请期待！' }
//   ]
}];


export default {
  namespaced: true,
  state: {
    config: {
      projectTitle: 'Fastbuild管理平台',
      projectName: 'fastbuild',
      webFramework: 'vue2',
      webUI: 'element',
      mobileFramework: 'uniapp',
      serverMode: 'single',
      packagePrefix: 'com.fastbuild',
      database: 'mysql',
      databaseVersion: '5.6',
      appList: []
    },
    appList: appList
  },
  mutations: {
    setAppList (state, appList) {
      state.appList = appList;
    },
    setConfig (state, config) {
      state.config = config;
    },
  },
  getters: {
  },
  actions: {
  }
}